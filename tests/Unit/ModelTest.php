<?php

namespace KDA\Tests\Unit;

use KDA\Laravel\Locale\Middleware\LocaleMiddleware;
use KDA\Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use KDA\Laravel\Locale\Facades\LocaleManager;
use Illuminate\Testing\TestResponse;

class ModelTest extends TestCase
{


  /** @test */
  function middlware_is_working()
  {
    $cookie_name =  config('kda.locale.cookie.name');

    //accept-language: fr-FR,fr;q=0.9,de;q=0.8,en;q=0.7,sv;q=0.6,it;q=0.5
    LocaleManager::availableLocales(['fr', 'en']);


    $request = Request::create('/test', 'POST', [], [], [], [
      'HTTP_ACCEPT_LANGUAGE' => 'fr-FR,fr;q=0.9,de;q=0.8,en;q=0.7,sv;q=0.6,it;q=0.5',
    ]);

    $middleware = new LocaleMiddleware();
    $response =   new TestResponse($middleware->handle($request, function () {
      return new response('');
    }));


    $this->assertEquals('fr', $this->app->getLocale());
    $response->assertCookie($cookie_name, 'fr', false);
    $response->assertCookieNotExpired($cookie_name);
  }

  /** @test */
  function fallback_is_working()
  {
    $cookie_name =  config('kda.locale.cookie.name');

    $request = Request::create('/test', 'POST', [], [], [], [
      'HTTP_ACCEPT_LANGUAGE' => 'fr-FR,fr;q=0.9,de;q=0.8,',
    ]);

    $middleware = new LocaleMiddleware();
    $response =  new TestResponse($middleware->handle($request, function () {
      return response('');
    }));

    $this->assertEquals('en', $this->app->getLocale());
    $response->assertStatus(200);
    $response->assertCookie($cookie_name, 'en', false);
    $response->assertCookieNotExpired($cookie_name);
  }

  /** @test */
  function disabling_cookie_is_working()
  {
    $cookie_name =  config('kda.locale.cookie.name');
    config()->set('kda.locale.cookie.enabled', false);
    //accept-language: fr-FR,fr;q=0.9,de;q=0.8,en;q=0.7,sv;q=0.6,it;q=0.5
    LocaleManager::availableLocales(['fr', 'en']);


    $request = Request::create('/test', 'POST', [], [], [], [
      'HTTP_ACCEPT_LANGUAGE' => 'fr-FR,fr;q=0.9,de;q=0.8,en;q=0.7,sv;q=0.6,it;q=0.5',
    ]);

    $middleware = new LocaleMiddleware();
    $response =   new TestResponse($middleware->handle($request, function () {
      return new response('');
    }));

    $response->assertStatus(200);

    $this->assertEquals('fr', $this->app->getLocale());
    $response->assertCookieMissing($cookie_name, 'fr', false);
  }


  /** @test */
  function query_string_overrides_headers()
  {
    $cookie_name =  config('kda.locale.cookie.name');
    LocaleManager::availableLocales(['fr', 'en', 'de']);


    $request = Request::create('/test?lang=de', 'GET', [], [], [], [
      'HTTP_ACCEPT_LANGUAGE' => 'fr-FR,fr;q=0.9,de;q=0.8,en;q=0.7,sv;q=0.6,it;q=0.5',
    ]);

    $middleware = new LocaleMiddleware();
    $response =   new TestResponse($middleware->handle($request, function () {
      return new response('');
    }));

    $response->assertStatus(200);
    $this->assertEquals('de', $this->app->getLocale());
  }


  /** @test */
  function query_string_overrides_cookies()
  {
    $cookie_name =  config('kda.locale.cookie.name');
    LocaleManager::availableLocales(['fr', 'en', 'de']);


    $request = Request::create('/test?lang=de', 'GET', [], [
      $cookie_name => 'fr'
    ], [], [
      'HTTP_ACCEPT_LANGUAGE' => 'fr-FR,fr;q=0.9,de;q=0.8,en;q=0.7,sv;q=0.6,it;q=0.5',
    ]);

    $middleware = new LocaleMiddleware();
    $response =   new TestResponse($middleware->handle($request, function () {
      return new response('');
    }));

    $response->assertStatus(200);
    $this->assertEquals('de', $this->app->getLocale());
    $response->assertCookie($cookie_name, 'de', false);
    $response->assertCookieNotExpired($cookie_name);
  }

  /** @test */
  function empty_query_string_doesnt_overrides_cookies()
  {
    $cookie_name =  config('kda.locale.cookie.name');
    LocaleManager::availableLocales(['fr', 'en', 'de']);


    $request = Request::create('/test?lang=', 'GET', [], [
      $cookie_name => 'fr'
    ], [], [
      'HTTP_ACCEPT_LANGUAGE' => 'fr-FR,fr;q=0.9,de;q=0.8,en;q=0.7,sv;q=0.6,it;q=0.5',
    ]);

    $middleware = new LocaleMiddleware();
    $response =   new TestResponse($middleware->handle($request, function () {
      return new response('');
    }));

    $response->assertStatus(200);
    $this->assertEquals('fr', $this->app->getLocale());
    $response->assertCookie($cookie_name, 'fr', false);
    $response->assertCookieNotExpired($cookie_name);
  }


  /** @test */
  function removed_lang_in_cookie_is_overriden()
  {
    $cookie_name =  config('kda.locale.cookie.name');
    LocaleManager::availableLocales(['fr', 'en', 'de']);


    $request = Request::create('/test?lang=sv', 'GET', [], [
      $cookie_name => 'sv'
    ], [], [
      'HTTP_ACCEPT_LANGUAGE' => 'fr-FR,fr;q=0.9,de;q=0.8,en;q=0.7,sv;q=0.6,it;q=0.5',
    ]);

    $middleware = new LocaleMiddleware();
    $response =   new TestResponse($middleware->handle($request, function () {
      return new response('');
    }));

    $response->assertStatus(200);
    $this->assertEquals('fr', $this->app->getLocale());
    $response->assertCookie($cookie_name, 'fr', false);
    $response->assertCookieNotExpired($cookie_name);
  }
}
