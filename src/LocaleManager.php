<?php

namespace KDA\Laravel\Locale;


use KDA\Laravel\Locale\Concerns\HttpLocale;
use KDA\Laravel\Locale\Concerns\Cookie;
use KDA\Laravel\Locale\Concerns\Locale;
use KDA\Laravel\Locale\Concerns\Querystring;

class LocaleManager
{
    use HttpLocale;
    use Cookie;
    use Querystring;
    use Locale;

    protected array $availableLocales = ['en'];
    
    public function __construct(){
        $this->setUp();
    }

    public function setUp()
    {
        $this->querystringKey(config('kda.locale.querystring.key', 'lang'))
        ->querystringEnabled(config('kda.locale.querystring.enabled', false))
        ->fallbackLocale(config('kda.locale.fallback','en'))
        ->availableLocales(config('kda.locale.available',['en']))
        ->cookieEnabled(config('kda.locale.cookie.enabled', true))
        ->cookieName(config('kda.locale.cookie.name', 'kda-locale'))
        ->cookieExpiration(intval(config('kda.locale.cookie.expiration', 60 * 60 * 24 * 365)));
    }

   

}
