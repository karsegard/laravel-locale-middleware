<?php

namespace KDA\Laravel\Locale\Concerns;

trait Querystring
{
    protected bool $querystring_enabled = false;
    protected string $querystring_key = 'lang';


    public function querystringKey(string $key):static{

        $this->querystring_key = $key;
        return $this;
    }

    public function querystringEnabled(string $enabled):static{

        $this->querystring_enabled = $enabled;
        return $this;
    }

    public function getQuerystringKey(): string
    {
        return $this->querystring_key;
    }
    public function isQueryStringEnabled(): bool
    {
        return $this->querystring_enabled;
    }
}
