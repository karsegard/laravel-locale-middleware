<?php

namespace KDA\Laravel\Locale\Concerns;

trait Locale
{
    protected string $fallback_locale = 'en';
    protected array $available_locales = ['en'];


    public function fallbackLocale(string $lang):static{

        $this->fallback_locale = $lang;
        return $this;
    }

    public function availableLocales(array $locales):static{

        $this->available_locales = $locales;
        return $this;
    }

    public function getAvailableLocales(): array
    {
        return $this->available_locales;
    }

    public function getFallbackLocale()
    {
        return $this->fallback_locale;
    }
    
    public function isAvailable($lang): bool
    {
        return in_array($lang, $this->getAvailableLocales());
    }

  
}
