<?php
namespace KDA\Laravel\Locale\Concerns;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
trait HttpLocale
{

    public function getHttpLocale(Request $request): ?string
    {
        $headers = $request->headers;
        if ($lang = $headers->get('Accept-Language')) {
            $locales = $this->parseHttpLocale($lang);

            $availables = $this->getAvailableLocales();
            foreach ($locales as $locale) {
                if (in_array($locale['locale'], $availables)) {
                    return $locale['locale'];
                }
            }
        }

        return null;
    }

    protected function parseHttpLocale($string)
    {
        $list = explode(',', $string);

        return  Collection::make($list)
            ->map(function ($locale) {
                $parts = explode(';', $locale);

                $mapping['locale'] = trim($parts[0]);

                if (isset($parts[1])) {
                    $factorParts = explode('=', $parts[1]);

                    $mapping['factor'] = $factorParts[1];
                } else {
                    $mapping['factor'] = 1;
                }

                return $mapping;
            })
            ->sortByDesc(function ($locale) {
                return $locale['factor'];
            });
    }

}

