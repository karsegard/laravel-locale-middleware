<?php

namespace KDA\Laravel\Locale\Concerns;

trait Cookie
{

    protected int $cookie_expiration =  60 * 60 * 24 * 365;
    protected bool $cookie_enabled = true;
    protected string $cookie_name = 'kda-locale';

    public function cookieEnabled(bool $enabled): static
    {

        $this->cookie_enabled = $enabled;
        return $this;
    }

    public function cookieExpiration(int $expiration): static
    {

        $this->cookie_expiration = $expiration;
        return $this;
    }

    public function cookieName(string $cookie_name): static
    {

        $this->cookie_name = $cookie_name;
        return $this;
    }

    public function isCookieEnabled(): bool
    {
        return $this->cookie_enabled;
        return config('kda.locale.cookie.enabled', true);
    }
    public function getCookieName(): string
    {
        return $this->cookie_name;
        return config('kda.locale.cookie.name', 'kda-locale');
    }
    public function getCookieExpiration(): int
    {
        return $this->cookie_expiration;
        return intval(config('kda.locale.cookie.expiration', 60 * 60 * 24 * 365));
    }
}
