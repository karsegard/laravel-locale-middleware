<?php

namespace KDA\Laravel\Locale;

use KDA\Laravel\Locale\Facades\LocaleManager as Facade;
use KDA\Laravel\Locale\LocaleManager as Library;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasConfig;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;

    protected $packageName = 'laravel-locale';

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    // trait \KDA\Laravel\Traits\HasConfig;
    //    registers config file as
    //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';

    protected $configs = [
        'kda/locale.php' => 'kda.locale',
    ];

    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }

    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }

    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
