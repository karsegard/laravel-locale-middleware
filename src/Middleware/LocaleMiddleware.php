<?php

namespace KDA\Laravel\Locale\Middleware;

use Closure;
use KDA\Laravel\Locale\Facades\LocaleManager;

class LocaleMiddleware
{
    public function handle($request, Closure $next)
    {
     
        $lang = LocaleManager::isCookieEnabled() 
                    && LocaleManager::isAvailable($request->cookie(LocaleManager::getCookieName())) 
                        ? $request->cookie(LocaleManager::getCookieName())
                        : null;

        $lang = LocaleManager::isQueryStringEnabled() 
                    && !blank($request->query(LocaleManager::getQuerystringKey())) 
                    &&  LocaleManager::isAvailable($request->query(LocaleManager::getQuerystringKey())) 
                        ? $request->query(LocaleManager::getQuerystringKey()) 
                        : $lang;

        $lang ??= LocaleManager::getHttpLocale($request);
        $lang ??= LocaleManager::getFallbackLocale();

        app()->setLocale($lang);

        if(LocaleManager::isCookieEnabled()){
            return $next($request)->cookie(LocaleManager::getCookieName(),$lang,LocaleManager::getCookieExpiration());
        }
        
        return $next($request);
    }
}
