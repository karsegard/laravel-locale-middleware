<?php

namespace KDA\Laravel\Locale\Facades;

use Illuminate\Support\Facades\Facade;

class LocaleManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
