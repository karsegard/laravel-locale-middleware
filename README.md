# kda/laravel-locale

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-locale.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-locale)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-locale.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-locale)


This will retrieve the browser or client locale and apply the first available locale as current language.

You can override the language by calling any url with the following querystring `http://yourhost.test/?lang=fr


## Installation

You can install the package via composer:

```bash
composer require kda/laravel-locale
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="\KDA\Laravel\Locale\ServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
];
```

## Usage

In the boot config of your provider, define the available locales like this

```php
 
    public function boot()
    {
        LocaleManager::availableLocales(['fr','en','de']);
   
    }
```

include it in your `app/Http/Kernel.php`  either by updating ` protected $middleware ` or `protected $middlewareGroups`

```php
    /**
     * The application's route middleware groups.
     *
     * @var array<string, array<int, class-string|string>>
     */
    protected $middlewareGroups = [
        'web' => [
          ///
             LocaleMiddleware::class
        ],

    ];
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](SECURITY.md) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](CONTRIBUTORS.md)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
